var $ = jQuery;

$(document).on('facetwp-refresh', function() {
    if (FWP.loaded) {
        $('.facetwp-facet .facet-inner').hide().before('<div class="facetwp-loading" />').nextAll('.facetwp-toggle').hide();
    }
});

function fixedHeaderTopSpacing(){
    if( $('.top-bar-1').length!=0 && ($('header.fl-page-header-primary').css('position') == "absolute" || $('header.fl-page-header-primary').css('position') == "fixed") ){
        $('header.fl-page-header-primary').css('top', $('.top-bar-1').outerHeight(true));
    }
}
$(document).ready(function() {
    fixedHeaderTopSpacing();
    $(window).resize(function(){
        fixedHeaderTopSpacing();
    });
});